<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [README](#markdown-header-readme)
    - [What is this repository for?](#markdown-header-what-is-this-repository-for)
    - [How do I get set up?](#markdown-header-how-do-i-get-set-up)
    - [Contribution guidelines](#markdown-header-contribution-guidelines)
    - [Who do I talk to?](#markdown-header-who-do-i-talk-to)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# README #

This README would normally document whatever steps are necessary to get your application up and running.

## What is this repository for? ###

- Quick summary
- Version
- [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

## How do I get set up? ###

- Summary of set up
- Configuration
- Dependencies
- Database configuration
- How to run tests
- Deployment instructions

## Contribution guidelines ###

- Writing tests
- Code review
- Other guidelines

## Who do I talk to? ###

- Repo owner or admin
- Other community or team contact
