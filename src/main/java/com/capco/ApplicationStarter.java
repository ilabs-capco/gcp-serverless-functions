package com.capco;

import com.google.cloud.functions.HttpFunction;
import com.google.cloud.functions.HttpRequest;
import com.google.cloud.functions.HttpResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ApplicationStarter implements HttpFunction {
  private static final Logger logger = LoggerFactory.getLogger(ApplicationStarter.class);

  @Override
  public void service(HttpRequest httpRequest, HttpResponse httpResponse) throws Exception {
    logger.info("Initiate the serverless functions");
    GCP gcp = new GCP();
    ConfigurationProperties configurationProperties = ConfigurationProperties.getInstance();
    SandpitProjectsManager sandpitProjectsManager =
        new SandpitProjectsManager(gcp, configurationProperties);
    sandpitProjectsManager.removeSandpitProjects(httpResponse);
  }
}
