package com.capco;

import java.io.InputStream;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConfigurationProperties {
  private static final Logger logger = LoggerFactory.getLogger(ConfigurationProperties.class);
  private static final Properties properties = new Properties();
  private static ConfigurationProperties configurationProperties = null;

  private ConfigurationProperties() {
    ClassLoader loader = Thread.currentThread().getContextClassLoader();
    try (InputStream configPropInputStream = loader.getResourceAsStream("config.properties"); ) {
      properties.load(configPropInputStream);
      logger.info("Configuration properties loaded");
    } catch (Exception e) {
      logger.error("Error occurred while trying to load config.properties file {}", e.getMessage());
      e.printStackTrace();
    }
  }

  public static ConfigurationProperties getInstance() {
    if (configurationProperties == null) {
      logger.info("Creating a new instance of ConfigurationProperties");
      configurationProperties = new ConfigurationProperties();
    }
    return configurationProperties;
  }

  public String getString(String key) {
    return properties.getProperty(key);
  }

  public Object getObject(Object key) {
    return properties.get(key);
  }

  public boolean contains(String value) {
    return properties.contains(value);
  }

  public boolean containsValue(String value) {
    return properties.contains(value);
  }

  public boolean containsKey(String key) {
    return properties.containsKey(key);
  }
}
