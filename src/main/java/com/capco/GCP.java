package com.capco;

import com.google.api.resourcenames.ResourceName;
import com.google.cloud.resourcemanager.v3.FolderName;
import com.google.cloud.resourcemanager.v3.Project;
import com.google.cloud.resourcemanager.v3.ProjectName;
import com.google.cloud.resourcemanager.v3.ProjectsClient;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GCP {
  Logger logger = LoggerFactory.getLogger(GCP.class);

  public Project getThisProject(String projectId) {
    try (ProjectsClient projectsClient = ProjectsClient.create()) {
      return projectsClient.getProject(projectId);
    } catch (Exception e) {
      logger.error("Error when trying to obtain project with id: {}", projectId);
      e.printStackTrace();
    }
    return null;
  }

  public List<Project> getListOfProjectsUnderFolder(String folderId) {
    List<Project> projectList = new ArrayList<>();
    try (ProjectsClient projectsClient = ProjectsClient.create()) {
      ResourceName parent = FolderName.of(folderId);
      for (Project project : projectsClient.listProjects(parent).iterateAll()) {
        projectList.add(project);
      }
    } catch (Exception e) {
      logger.error(
          "Error while trying to fetch the list of projects from a folder with id: {}", folderId);
      e.printStackTrace();
    }
    return projectList;
  }

  public boolean deleteProject(String projectName) {
    try (ProjectsClient projectsClient = ProjectsClient.create()) {
      String name = ProjectName.of(projectName).toString();
      Project response = projectsClient.deleteProjectAsync(name).get();
      return response == null ? false : true;
    } catch (Exception e) {
      logger.error("Error when trying to delete a project: {}", projectName);
      e.printStackTrace();
    }
    return false;
  }
}
