package com.capco;

import com.google.cloud.functions.HttpResponse;
import com.google.cloud.resourcemanager.v3.Project;
import java.io.BufferedWriter;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SandpitProjectsManager {
  private static final Logger logger = LoggerFactory.getLogger(SandpitProjectsManager.class);
  private final ConfigurationProperties configProp;
  private final GCP gcp;
  private final String SANDPIT_ID_KEY = "sandpitID";
  public final String DELETION_THRESHOLD_KEY = "deletionThreshold";

  public SandpitProjectsManager(GCP gcp, ConfigurationProperties configProp) {
    logger.info("Creating a new instantiate SandpitProjectsManager");
    this.gcp = gcp;
    this.configProp = configProp;
  }

  public void removeSandpitProjects(HttpResponse httpResponse) {
    String sandpitDirectoryID = configProp.getString(SANDPIT_ID_KEY);
    List<Project> projectList = getListOfSandpitProjects(sandpitDirectoryID);
    int thresholdDays = Integer.valueOf(configProp.getString(DELETION_THRESHOLD_KEY));
    logger.info("The sandpitID: {}, threshold: {}", sandpitDirectoryID, thresholdDays);
    projectList.stream()
        .forEach(
            project -> {
              Instant creationDate =
                  Instant.ofEpochSecond(project.getCreateTimeOrBuilder().getSeconds());
              Instant dateForDeletion = creationDate.plus(thresholdDays, ChronoUnit.DAYS);
              Instant currentDate = Instant.now();
              logger.info(
                  "Project ID: {} \n"
                      + "Created at: {} \n"
                      + "To be deleted: {} \n"
                      + "current time: {} \n"
                      + "to delete: {}",
                  project.getProjectId(),
                  creationDate,
                  dateForDeletion,
                  currentDate,
                  currentDate.isAfter(dateForDeletion));
            });
    writeResponseProjectIDs(httpResponse, projectList);
  }

  private List<Project> getListOfSandpitProjects(String sandpitDirectoryID) {
    return gcp.getListOfProjectsUnderFolder(sandpitDirectoryID);
  }

  private void writeResponseProjectIDs(HttpResponse httpResponse, List<Project> projectList) {
    try (BufferedWriter writer = httpResponse.getWriter()) {
      for (Project project : projectList) {
        writer.write("Project ID: " + project.getProjectId());
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
